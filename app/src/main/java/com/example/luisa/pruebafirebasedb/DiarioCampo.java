package com.example.luisa.pruebafirebasedb;

public class DiarioCampo {

    private String id;
    private String clase;
    private String seccion;
    private String asignatura;

    public DiarioCampo() {
    }

    public DiarioCampo(String id, String clase, String seccion, String asignatura) {
        this.id = id;
        this.clase = clase;
        this.seccion = seccion;
        this.asignatura = asignatura;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }
}
