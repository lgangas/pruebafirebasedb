package com.example.luisa.pruebafirebasedb;

import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    private EditText txtClase;
    private Spinner cbSeccion;
    private Spinner cbAsignatura;
    private Button btnRegistrar;
    private DatabaseReference dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataBase = FirebaseDatabase.getInstance().getReference("DiarioCampo");

        txtClase = findViewById(R.id.txtClase);
        cbSeccion = findViewById(R.id.cbSeccion);
        ArrayAdapter<CharSequence> adapterSeccion = ArrayAdapter.createFromResource(this,R.array.secciones,android.R.layout.simple_spinner_item);
        adapterSeccion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbSeccion.setAdapter(adapterSeccion);

        cbAsignatura = findViewById(R.id.cbAsignatura);
        ArrayAdapter<CharSequence> adapterAsignatura = ArrayAdapter.createFromResource(this,R.array.asignaturas,android.R.layout.simple_spinner_item);
        adapterAsignatura.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbAsignatura.setAdapter(adapterAsignatura);

        btnRegistrar = findViewById(R.id.btnRegistrar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarClase();
            }
        });
    }
    public void registrarClase(){
        String seccion = cbSeccion.getSelectedItem().toString();
        String asginatura = cbAsignatura.getSelectedItem().toString();
        String clase = txtClase.getText().toString();

        if(!TextUtils.isEmpty(clase)){
            String id = dataBase.push().getKey();
            DiarioCampo diario = new DiarioCampo(id,clase,seccion,asginatura);
            dataBase.child("Lecciones").child(id).setValue(diario);

            Toast.makeText(this, "Registro existoso.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Escriba un tema.", Toast.LENGTH_SHORT).show();
        }
    }
}

